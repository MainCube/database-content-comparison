#include "SpookyV2.cpp"
#include "MurmurHash3.cpp"
#include "xxHash.cpp"
#include "farmhash.cc"
#include "MetroHash128.cpp"
#include "MetroHash64.cpp"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <pqxx/pqxx> 
#include <unistd.h>
#include <chrono>
#include <algorithm>

using namespace std;
using namespace pqxx;


std::string generateRandomText(int length){
    static const char alphabet[] =
        "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "0123456789";
    const int alphabetSize = sizeof(alphabet) - 1;

    std::string text;
    text.reserve(length);
    for (int i = 0; i < length; ++i)
    {
        text += alphabet[rand() % alphabetSize];
    }
    return text;
}

void createTable(connection &c, connection &c2, int amountTestData, int lengthTestData) {

    char * sql = (char*) "CREATE TABLE IF NOT EXISTS data (value text)";
    work txn(c);
    work txn2(c2);
    txn.exec( sql );
    txn2.exec( sql );


    for (int i = 1; i <= amountTestData; i++){
    string text = generateRandomText(lengthTestData);
    txn.exec("INSERT INTO data (value) VALUES ('" + text + "')");
    txn2.exec("INSERT INTO data (value) VALUES ('" + text + "')");
    }
    txn.commit();
    txn2.commit();

}

string getSpookyHash128(const char *input){
    uint64 hash1, hash2;

    SpookyHash spooky;
    spooky.Init(0,0);
    spooky.Update(input, strlen(input));
    spooky.Final(&hash1, &hash2);

    stringstream ss;
    ss << std::hex << hash1 << hash2;
    return ss.str();
}

string getSpookyHash64(const char *input){
    uint64 hash;
    SpookyHash spooky;
    hash = spooky.Hash64(input, strlen(input), 0);

    stringstream ss;
    ss << std::hex << hash;
    return ss.str();
}

string getSpookyHash32(const char *input){
    uint32 hash;
    SpookyHash spooky;
    hash = spooky.Hash32(input, strlen(input), 0);

    stringstream ss;
    ss << std::hex << hash;
    return ss.str();
}

string getMurmurHash128(const char* input) {
    uint64_t hash[2];

    MurmurHash3_x64_128(input, strlen(input), 0, hash);

    std::stringstream ss;
    ss << std::hex << hash[0] << hash[1];
    return ss.str();
}

string getXxHash128(const char* input) {
    XXH128_hash_t hash = XXH3_128bits_withSeed(input, strlen(input), 0);

    std::stringstream ss;
    ss << std::hex << hash.high64 << hash.low64;
    return ss.str();
}

string getXxHash64(const char* input) {
    XXH64_hash_t hash = XXH3_64bits_withSeed(input, strlen(input), 0);

    std::stringstream ss;
    ss << std::hex << hash;
    return ss.str();
}


std::string getFarmHash128(const char* input) {
    const uint128_t hash = Hash128WithSeed(input, strlen(input), {0, 0});

    std::stringstream ss;
    ss << std::hex << hash.first << hash.second;
    return ss.str();
}

std::string getFarmHash64(const char* input) {
    const uint64_t hash = Hash64WithSeed(input, strlen(input), 0);
    std::stringstream ss;
    ss << std::hex << hash;
    return ss.str();
}

std::string getFarmHash32(const char* input) {
    const uint32_t hash = Hash32WithSeed(input, strlen(input), 0);
    std::stringstream ss;
    ss << std::hex << hash;
    return ss.str();
}


string getMetroHash128(const char* input) {
    uint64_t hash[2];

    metrohash128_2(reinterpret_cast<const uint8_t*>(input), strlen(input), 0, reinterpret_cast<uint8_t*>(hash));

    std::stringstream ss;

    ss << std::hex << hash[0] << hash[1];
    return ss.str();
}

string getMetroHash64(const char* input) {
    uint64_t hash[1];

    metrohash128_2(reinterpret_cast<const uint8_t*>(input), strlen(input), 0, reinterpret_cast<uint8_t*>(hash));

    std::stringstream ss;

    ss << std::hex << hash[0];
    return ss.str();
}




//g++ main.cpp -lpqxx -lpq
//./a.out
int main() {

std::cout << getSpookyHash128("test") << endl;
std::cout << getSpookyHash64("test") << endl;
std::cout << getSpookyHash32("test") << endl;

std::cout << getMurmurHash128("test") << endl;

std::cout << getXxHash128("test") << endl;
std::cout << getXxHash64("test") << endl;

std::cout << getFarmHash128("test") << endl;
std::cout << getFarmHash64("test") << endl;
std::cout << getFarmHash32("test") << endl;

std::cout << getMetroHash128("test") << endl;
std::cout << getMetroHash64("test") << endl;


//------------------------------------------------------------------------Vorbereitung: Datenbanken verbinden------------------------------------------------------------------------
    try {
        connection C("dbname = database1 user = postgres password = test \
        hostaddr = 127.0.0.1 port = 5432");
        if (C.is_open()) {
             std::cout << "Opened database1 successfully: " << C.dbname() << endl;
        } else {
             std::cout << "Can't open database1" << endl;
            return 1;
        }

        connection C2("dbname = database2 user = postgres password = test \
        hostaddr = 127.0.0.1 port = 5432");
        if (C2.is_open()) {
             std::cout << "Opened database2 successfully: " << C2.dbname() << endl;
        } else {
             std::cout << "Can't open database2" << endl;
            return 1;
        }

//------------------------------------------------------------------------Vorbereitung: Variablen anpassen------------------------------------------------------------------------
        int anzahlTupel = 50000;
        //int erwartungswertTupelLänge[] = {1, 12500 ,25000, 37500, 50000};
        int erwartungswertTupelLänge[] = {1};
        int Wiederholungen = 50;
        string (*HashFunktionen[11])(char const*) = {getSpookyHash128, getSpookyHash64, getSpookyHash32, getMurmurHash128, getXxHash128, getXxHash64, getFarmHash128, getFarmHash64, getFarmHash32, getMetroHash128, getMetroHash64};
        string HashFunktionenNamen[11] = {"SpookyHash128", "SpookyHash64", "SpookyHash32", "MurmurHash128", "XxHash128", "XxHash64", "FarmHash128", "FarmHash64", "FarmHash32", "MetroHash128", "MetroHash64"};

        int results_tuple[sizeof(erwartungswertTupelLänge)/sizeof(erwartungswertTupelLänge[0])][sizeof(HashFunktionen)][Wiederholungen];
//------------------------------------------------------------------------Vorbereitung: Datenbank füllen und laden------------------------------------------------------------------------
        //Lege Neue TestDaten an und lösche ggf. die alten
        for (int i=0; i<sizeof(erwartungswertTupelLänge)/sizeof(erwartungswertTupelLänge[0]); i++){
        try{
            work txn3(C);
            result result3 = txn3.exec( "DROP TABLE data" );
            txn3.commit();

            work txn4(C2);
            result result4 = txn4.exec( "DROP TABLE data" );
            txn4.commit();

            createTable(C, C2, anzahlTupel, erwartungswertTupelLänge[i]);
        } catch(const std::exception& e){
            createTable(C, C2, anzahlTupel, erwartungswertTupelLänge[i]);
            std::cout << "Datenbanken wurden bereits gefüllt" << endl;
        }

        //Lade Daten aus Datenbank
        char *sql = (char*) "SELECT * FROM data";
        work W(C);
        result result1 = W.exec( sql );
        W.commit();
        work W2(C2);
        result result2 = W2.exec( sql );
        W2.commit();

//------------------------------------------------------------------------Experiment: Jeden Tupel hashen und vergleichen------------------------------------------------------------------------
        for(int j=0; j<Wiederholungen; j++){
            //Iteriere Hash-Funktionen
            for(int k=0; k<sizeof(HashFunktionen)/sizeof(HashFunktionen[0]); k++){

                string hashedResult1[anzahlTupel];
                string hashedResult2[anzahlTupel];

                auto start = std::chrono::high_resolution_clock::now();

                //Hashe jedes Tupel in beiden Datenbanken und vergleiche
                for(int l=0; l<anzahlTupel; l++){
                    hashedResult1[l]= HashFunktionen[k]((result1[l][0].as<string>()).c_str());
                    hashedResult2[l]= HashFunktionen[k]((result2[l][0].as<string>()).c_str());
                    if(hashedResult1[l] != hashedResult2[l]){
                        std::cout << "Datenbanken ab hier nicht gleich" << endl;
                    }
                }   

                //speicher die Zeit
                auto end = std::chrono::high_resolution_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
                results_tuple[i][k][j] = duration.count();
            }
        }
        }

//------------------------------------------------------------------------Ergebnisse Ausgeben-----------------------------------------------------------------------
        for(int i=0; i<sizeof(erwartungswertTupelLänge)/sizeof(erwartungswertTupelLänge[0]); i++){
            std::cout << "----------Tupellänge: " << erwartungswertTupelLänge[i] << "----------" << endl;
            for(int j=0; j<sizeof(HashFunktionen)/sizeof(HashFunktionen[0]); j++){
                std::cout << HashFunktionenNamen[j] << endl;
                for(int k=0; k<Wiederholungen; k++){
                    std::cout << results_tuple[i][j][k] << endl;
                }
            }
        }

//-----------------------------------------------------------------------------------------------------------------------------------------------

        C.disconnect();
        C2.disconnect();

    
    } catch (const std::exception &e) {
      cerr << e.what() << std::endl;
      return 1;
   }

    return 0;
}
